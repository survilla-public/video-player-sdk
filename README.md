Video Player SDK
=====================

This repository contains basic example for Survilla live video player. Player can be used to embed live video from [Survilla and NetRex Cloud Video Monitoring](https://netrex.cz/en) to your website. Player uses RTSP over WebSocket.

# Using
To use this example copy the content of `<head>` from `player.html` into your website. Make sure your website has two `<input>` fields with ID  `#alias_name`, `#websocket_url` and one `<button>` with ID `#btn-play`. Do not forget to add `streamedian.min.js` into your static files.

To get `alias_name` and `websocket_url` you need to use your cloud account API. Mind that the `alias_name` expires after 30 seconds or after first usage. You need to refresh it every time when starting a new connection. More info about API authorization and schema can be found in your cloud account under API section. API uses [GraphQL](https://graphql.org/) technology.

# Getting stream URL and alias using cloud API
Fetch list of cameras and corresponding camera_id:

```
query {
  cameraList {
    name
    node_id
    }  
  }
}
```
Fetch streaming profiles for given camera:
```
query {
   camera(node_id:<NODE_ID>){
    stream_profiles{
      stream_profile_id
      width
      height
    }
  }
}
```
Fetch url and alias_name to be used with this player:
```
{
  camera(node_id: <NODE_ID>) {
    live_view(stream_profile_id:<STREAM_PROFILE_ID>) {
      remote{
        rtspwss {
          alias_name
          ws {
            url
          }
        }
      }
    }
  }
}  
```
